// "OR" operator: Find users with the letter s in their first name or d in their last name.
	// show only the firstName and lastName fields and hide the _id field

db.users.find( 
	{ $or: [ 
		{ "firstName": {$regex: "s", $options: 'i'}}, 
		{ "lastName": {$regex: "d", $options: 'i'}} ] 
	},
	{
		"_id": 0,
		"firstName": 1,
		"lastName": 1,
	} 
);

// returns 2 documents



// "AND" operator: HR Department and their age is greater than or equal to 70

db.users.find( 
	{ $and: [ 
		{ "department": "HR"}, 
		{ "age": {$gte: 70}} ]
	}
);

// returns 2 documents



// Find users with the letter e in their first name and has an age of less than or equal to 30.
	// a. Use the $and, $regex and $lte operators

db.users.find( 
	{ $and: [ 
		{ "firstName": {$regex: "e", $options: 'i'}}, 
		{ "age": {$lte: 30}} ]
	}
);

// returns 1 document